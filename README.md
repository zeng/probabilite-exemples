# Exemples en Probabilité

Simulation et illustration des exemples de probabilites, L3, M1

## Installer les dependances python
> conda env create -f environment.yml

> conda activate proba

## Lander la commande suivante dans le répertoire principal
>    jupyter-notebook

## Test with Binder

[![Binder](https://static.mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fzeng%2Fprobabilite-exemples/master)
