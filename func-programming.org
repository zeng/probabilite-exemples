* Functional programming in Python

** Iterators
=iter()= return an iterator of its argument
#+begin_src jupyter-python :session py :async yes :display plain
L=[1,2,3]
it=iter(L)
it
it.__next__()
#+end_src

#+RESULTS:
: 1


